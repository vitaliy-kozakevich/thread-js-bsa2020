/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import UpdatePost from 'src/components/UpdatePost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  editPost,
  deletePost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  notUserId: undefined,
  likedByUserId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  editPost: editPostBody,
  deletePost: removePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle
}) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  // const [socket] = useState(io(address));
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [updatePostId, setUpdatePostId] = useState(undefined);
  const [updatePostBody, setUpdatePostBody] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showNotOwnPosts, setShowNotOwnPosts] = useState(false);
  const [showOnlyOwnLiked, setShowOnlyOwnLiked] = useState(false);

  // useEffect(() => {
  //   socket.on('like', () => {

  //   });
  //   return () => {
  //     socket.close();
  //   }
  // }, [input])

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowNotOwnPosts = () => {
    setShowNotOwnPosts(!showNotOwnPosts);
    postsFilter.notUserId = showNotOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOnlyLiked = () => {
    setShowOnlyOwnLiked(!showOnlyOwnLiked);
    postsFilter.likedByUserId = showOnlyOwnLiked ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const updatePost = (id, body) => {
    setUpdatePostId(id);
    setUpdatePostBody(body);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        {
          !showNotOwnPosts ? (
            <Checkbox
              toggle
              label="Show only my posts"
              checked={showOwnPosts}
              onChange={toggleShowOwnPosts}
              style={{ marginRight: 10 }}
            />
          ) : null
        }
        <Checkbox
          toggle
          label="Hide my posts"
          checked={showNotOwnPosts}
          onChange={toggleShowNotOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only liked by me"
          checked={showOnlyOwnLiked}
          onChange={toggleShowOnlyLiked}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            updatePost={updatePost}
            deletePost={removePost}
            key={post.id}
            userId={userId}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {updatePostId
        && (
          <UpdatePost
            headeTitle="Update Post"
            postId={updatePostId}
            postBody={updatePostBody}
            editPostBody={editPostBody}
            close={() => setUpdatePostId(undefined)}
          />
        )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  editPost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
