import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const editPost = (id, body) => async (dispatch, getRootState) => {
  const { id: postId, body: newPostBody } = await postService.editPost(id, body);

  const mapPost = post => {
    if (post.id === postId) {
      return {
        ...post,
        body: newPostBody
      };
    }
    return post;
  };

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapPost(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapPost(expandedPost)));
  }
};

export const deletePost = id => async (dispatch, getRootState) => {
  const data = await postService.deletePost(id);

  if (data && data.message) {
    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.filter(post => post.id !== data.id);

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === data.id) {
      dispatch(setExpandedPostAction(null));
    }
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = post => {
    if (post.reactionId === id) {
      return {
        ...post,
        likeCount: Number(post.likeCount) + diff,
        dislikeCount: Number(post.dislikeCount) - diff
      };
    }
    return {
      ...post,
      likeCount: Number(post.likeCount) + diff,
      reactionId: id
    };
  };

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1;

  const mapDislikes = post => {
    if (post.reactionId === id) {
      return {
        ...post,
        likeCount: Number(post.likeCount) - diff,
        dislikeCount: Number(post.dislikeCount) + diff
      };
    }
    return {
      ...post,
      dislikeCount: Number(post.dislikeCount) + diff,
      reactionId: id
    };
  };

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const editComment = (id, body) => async (dispatch, getRootState) => {
  const { id: commentId, body: newCommentBody } = await commentService.editComment(id, body);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: [...post.comments.map(c => {
      if (c.id === commentId) {
        return {
          ...c,
          body: newCommentBody
        };
      }
      return c;
    })]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const deleteComment = id => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(id);
  const data = await commentService.deleteComment(id);

  if (data && data.message) {
    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: [...post.comments.filter(c => c.id !== data.id)]
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
      ? post
      : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
      dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
  }
};
