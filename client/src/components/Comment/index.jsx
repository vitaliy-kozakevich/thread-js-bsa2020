import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Comment as CommentUI, Button, Label, Icon } from 'semantic-ui-react';
import UpdatePost from 'src/components/UpdatePost';
import { editComment, deleteComment } from 'src/containers/Thread/actions';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body, createdAt, user, userId, id },
  userId: activeUserId,
  editComment: updateCmnt,
  deleteComment: deleteCmnt
}) => {
  const [updateCommentId, seTupdateCommentId] = useState(undefined);
  const [updateCommentBody, setUpdateCommentBody] = useState(undefined);
  const isActiveUser = userId === activeUserId;

  const updateComment = (commentId, commentBody) => {
    seTupdateCommentId(commentId);
    setUpdateCommentBody(commentBody);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        {
          isActiveUser ? (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => updateComment(id, body)}>
              <Icon name="arrow up" />
            </Label>
          ) : null
        }
        {
          isActiveUser ? (
            <Button floated="right" color="red" onClick={() => deleteCmnt(id)}>Delete</Button>
          ) : null
        }
        { updateCommentId && (
          <UpdatePost
            headeTitle="Update Comment"
            postId={updateCommentId}
            postBody={updateCommentBody}
            editPostBody={updateCmnt}
            close={() => seTupdateCommentId(undefined)}
          />
        )}
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  editComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

const actions = {
  editComment,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(Comment);
