import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, TextArea, Button, Form } from 'semantic-ui-react';

import styles from './styles.module.scss';

function UpdatePost({ postId, postBody, editPostBody, close, headeTitle }) {
  const [body, setBody] = useState(postBody);

  const onChange = ({ target: { value } }) => {
    setBody(value);
  };

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await editPostBody(postId, { body });
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>{headeTitle}</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleEditPost}>
          <TextArea
            placeholder="Type post content"
            style={{ minHeight: 100 }}
            value={body}
            name="body"
            onChange={onChange}
          />
          <Button floated="right" color="blue" type="submit" style={{ marginTop: 20, marginBottom: 20 }}>Edit</Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
}

UpdatePost.propTypes = {
  postId: PropTypes.string.isRequired,
  postBody: PropTypes.string.isRequired,
  editPostBody: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  headeTitle: PropTypes.string.isRequired
};

export default UpdatePost;
