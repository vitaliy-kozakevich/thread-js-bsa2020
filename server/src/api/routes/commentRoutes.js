import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/:id', (req, res, next) => commentService.update(req.params.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteSoft(req.params.id)
    .then(() => res.send({ message: `Comment with id: ${req.params.id} has been deleted!`, id: req.params.id }))
    .catch(next));

export default router;
