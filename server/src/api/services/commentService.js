import commentRepository from '../../data/repositories/commentRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const update = (id, body) => commentRepository.updateById(id, body);

export const deleteSoft = id => commentRepository.deleteById(id);
