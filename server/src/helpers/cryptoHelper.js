const bcryptjs = require('bcryptjs');

const saltRounds = 10;

export const encrypt = data => bcryptjs.hash(data, saltRounds);

export const encryptSync = data => bcryptjs.hashSync(data, saltRounds);

export const compare = (data, encrypted) => bcryptjs.compare(data, encrypted);
